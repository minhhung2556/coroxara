import 'dart:io';

import 'package:coroxara/data/ad/constants.dart';
import 'package:coroxara/data/appdata/appdata_api.dart';
import 'package:coroxara/data/appdata/appdata_pref.dart';
import 'package:coroxara/data/appdata/appdata_repository.dart';
import 'package:coroxara/res/colors.dart';
import 'package:coroxara/res/strings.dart';
import 'package:coroxara/ui/router.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  setupDI();
  runApp(MyApp());
}

void setupDI() {
  GetIt.I.registerLazySingleton(() => AppDataApiProvider(""));
  GetIt.I.registerLazySingleton(() => AppDataPrefProvider());
  GetIt.I.registerLazySingleton(() => AppDataRepository(
      GetIt.I.get<AppDataApiProvider>(), GetIt.I.get<AppDataPrefProvider>()));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    FirebaseAdMob.instance.initialize(
        appId: Platform.isAndroid
            ? AdConstants.AD_MOB_APP_ID_ANDROID
            : AdConstants.AD_MOB_APP_ID_IOS);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: Router.navigatorKey,
      onGenerateRoute: Router.generateRoute,
      initialRoute: Router.initialRoute,
      title: AssetStrings.appName,
      theme: ThemeData(
          primaryColor: AssetColors.primaryColor,
          accentColor: AssetColors.accentColor),
    );
  }
}
