extension ParseString on String {
  double toDouble() {
    return double.parse(this);
  }
}

extension ParseDate on DateTime {
  String get ddMMyyyy {
    return "${addZero(this.day)}/${addZero(this.month)}/${addZero(this.year)}";
  }

  String get hhMM {
    return "${addZero(this.hour)}h${addZero(this.minute)}";
  }

  String addZero(int num) {
    return num < 10 ? "0$num" : "$num";
  }

  DateTime withZeroTime() {
    return DateTime(year, month, day);
  }
}
