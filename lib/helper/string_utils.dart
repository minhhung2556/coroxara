import 'package:coroxara/helper/logger.dart';
import 'package:intl/intl.dart';

bool isNotEmpty(String s) {
  return s != null && s.trim().length > 0;
}

bool isEmpty(String s) {
  return !isNotEmpty(s);
}

String removeExtension(String name) {
  try {
    return name.substring(0, name.lastIndexOf('.'));
  } catch (e) {
    return name;
  }
}

String downloadUrlFromDriveId(String id) {
  return "https://drive.google.com/uc?export=download&id=$id";
}

String googleDocsViewerFromDriveId(String id) {
  return "https://drive.google.com/file/d/$id/view";
}

String withoutId(String str) {
  if (str == null) return null;
  int index;
  if (str.contains("/")) {
    index = str.indexOf("/");
  } else if (str.contains(")")) {
    index = str.indexOf(")");
  }
  if (index != null) {
    return str.substring(index + 1).trim();
  } else {
    return str;
  }
}

String mapToDiagnosisString(String content) {
  return content.replaceAll("/", "\n");
}

DateTime dateTimeFromString(String str, [String pattern = 'dd/MM/yyyy']) {
  if (isEmpty(str)) return DateTime.now();
  final _dateFormat = DateFormat(pattern);
  try {
    return _dateFormat.parse(str);
  } catch (e) {
    Log.d("", e);
  }
  return DateTime.now();
}
