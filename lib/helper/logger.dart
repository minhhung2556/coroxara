import 'package:logger/logger.dart';

class Log {
  static final logger = Logger();

  static void init() {
    Logger.level = Level.debug;
  }

  static void i(dynamic str, [dynamic error, StackTrace stackTrace]) {
    logger.i(str, error, stackTrace);
  }

  static void d(dynamic str, [dynamic error, StackTrace stackTrace]) {
    logger.d(str, error, stackTrace);
  }
}
