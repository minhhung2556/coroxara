import 'package:flutter/material.dart';

class AssetImages {
  static const placeHolder = "asset/placeholder.png";
  static const homeTabs = <IconData>[
    Icons.home,
    Icons.description,
    Icons.local_hospital
  ];
  static const coronavirus = 'asset/coronavirus.png';
}
