class AssetStrings {
  static const appName = "Coroxara - Dịch viêm phổi cấp Corona";
  static const getAppDataFailMessage = "Không thể tải dữ liệu!";
  static const homeTabs = <String>['Trang chủ', 'Thông tin', 'Bệnh viện'];
  static const homeTabScreenTitles = <String>[
    'Thống kê',
    'Thông tin',
    'Bệnh viện'
  ];
  static const overviewItemNames = <String>[
    'Tử vong',
    'Nhiễm bệnh',
    'Khỏi bệnh',
  ];
  static const unknown = 'Đang cập nhật...';
}
