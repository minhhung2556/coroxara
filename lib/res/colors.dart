import 'package:flutter/material.dart';

class AssetColors {
  static const primaryColor = Color(0xFF09164f);
  static const controlColor = Color(0xFFa2eeff);
  static const accentColor = Color(0xFFdc3f48);
  static const background = Colors.white;
  static const textBlack = Colors.black;
  static const textGrey = Color(0xFFe3e3e3);
  static const homeBackground = Color(0xFF010038);
  static const overviewItemColors = <Color>[
    Color(0xFFcc2e43),
    Color(0xFFf39422),
    Color(0xFF21bf73),
  ];
  static const textDarkGrey = Color(0xFF545454);
}
