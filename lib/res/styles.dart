import 'package:coroxara/res/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AssetStyles {
  static const mediumBlack16 = TextStyle(
      fontSize: 16, fontWeight: FontWeight.bold, color: AssetColors.textBlack);

  static const mediumBlack20 = TextStyle(
      fontSize: 20, fontWeight: FontWeight.bold, color: AssetColors.textBlack);

  static const regularBlack14 = TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.normal,
      color: AssetColors.textBlack);

  static const boldBlack14 = TextStyle(
      fontSize: 14, fontWeight: FontWeight.bold, color: AssetColors.textBlack);

  static const boldBlack16 = TextStyle(
      fontSize: 16, fontWeight: FontWeight.bold, color: AssetColors.textBlack);

  static const regularBlack16 = TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.normal,
      color: AssetColors.textBlack);

  static const mediumWhite14 =
      TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Colors.white);

  static const mediumGrey14 = TextStyle(
      fontSize: 14, fontWeight: FontWeight.w400, color: AssetColors.textGrey);

  static const mediumPrimary14 = TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.bold,
      color: AssetColors.primaryColor);

  static const mediumPrimary16 = TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.bold,
      color: AssetColors.primaryColor);

  static const regularWhite16 = TextStyle(
      fontSize: 16, fontWeight: FontWeight.normal, color: Colors.white);

  static const homeTabScreenTitle = TextStyle(
      fontSize: 28, fontWeight: FontWeight.bold, color: AssetColors.textGrey);

  static const boldGrey24 = TextStyle(
      fontSize: 24, fontWeight: FontWeight.bold, color: AssetColors.textGrey);

  static const boldGrey12 = TextStyle(
      fontSize: 12, fontWeight: FontWeight.bold, color: AssetColors.textGrey);

  static const boldGrey14 = TextStyle(
      fontSize: 14, fontWeight: FontWeight.bold, color: AssetColors.textGrey);

  static const regularPrimaryColor16 = TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.normal,
      color: AssetColors.primaryColor);
}
