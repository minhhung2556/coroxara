import 'package:coroxara/data/base/repository.dart';
import 'package:coroxara/data/base/result.dart';
import 'package:coroxara/data/overview/overview_model.dart';

import 'overview_api.dart';

class OverviewRepository extends Repository {
  final OverviewApiProvider _api;

  OverviewRepository(this._api);

  Future<Result<OverviewData>> getOverviewData() async {
    try {
      final res = await _api.getOverviewData();
      return Result<OverviewData>.success(res);
    } catch (e) {
      return Result<OverviewData>.failure(e.message);
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
  }
}
