class OverviewData {
  final int totalCases;
  final int totalCriticalCases;
  final int totalDeaths;
  final int totalRecovered;

  OverviewData({
    this.totalCases,
    this.totalCriticalCases,
    this.totalDeaths,
    this.totalRecovered,
  });
}
