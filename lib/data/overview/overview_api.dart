import 'package:coroxara/data/base/exception.dart';
import 'package:coroxara/data/constants.dart';
import 'package:coroxara/data/network/api_provider.dart';
import 'package:coroxara/helper/logger.dart';
import 'package:coroxara/res/strings.dart';
import 'package:html/parser.dart';
import 'package:http/http.dart' as http;

import 'overview_model.dart';

class OverviewApiProvider extends ApiProvider {
  OverviewApiProvider() : super("");

  Future<OverviewData> getOverviewData() async {
    try {
      final response = await http.get(OVERVIEW_DATA_URL);
      final document = parse(response.body);
      final table = document.getElementsByClassName(
          "table table-striped table-bordered dataTable no-footer")[0];
      final rows = table.querySelectorAll('tr');
//      rows[1].querySelectorAll('td')[1].textContent
//      document.querySelector("#maincounter-wrap").children[1].textContent
//      " 28,365"
//      document.querySelector("#maincounter-wrap").children[2].children[0].textContent
//      "3,863"
//      document.querySelectorAll("#maincounter-wrap")[0].children[2].children[0].textContent
//      "3,863"
//      document.querySelectorAll("#maincounter-wrap")[1].children[1].children[0].textContent
//      "566"
//      document.querySelectorAll("#maincounter-wrap")[2].children[1].children[0].textContent
//      "1,428"

      /*final data = new List<TideReport>();
      for (int i = 0; i < tables.length; i++) {
        TideReport item = new TideReport();
        data.add(item);
        final table = tables[i];
        // read date
        final e = table
            .querySelector("thead")
            .querySelector("tr")
            .querySelector("th")
            .querySelector("h2");
        String dateAll = e.text;
        String date = dateAll.substring(
            dateAll.indexOf("Dương Lịch ") + 11, dateAll.indexOf(" (Âm Lịch "));
        String dateLunar = dateAll.substring(
            dateAll.indexOf(" (Âm Lịch ") + 10, dateAll.length - 1);
        debugPrint(tag +
            ": dateAll=" +
            dateAll +
            ", date=" +
            date +
            ", dateLunar=" +
            dateLunar);
        item.date = date;
        item.dateLunar = dateLunar;
        item.records = List<Tide>();
        // records
        List<Element> rows =
            table.querySelector("tbody").querySelectorAll("tr");
        for (int j = 0; j < rows.length; j++) {
          Tide tide = new Tide();
          tide.date = item.date;
          tide.dateLunar = item.dateLunar;
          item.records.add(tide);

          List<Element> cols = rows[j].querySelectorAll("td");
          tide.place = cols[0].text;
          tide.highTimes = List<String>();
          tide.highTimes.add(cols[1].text);
          tide.highTimes.add(cols[5].text);
          tide.highValues = List<double>();
          tide.highValues.add(parseDouble(cols[2].text));
          tide.highValues.add(parseDouble(cols[6].text));
          tide.lowTimes = List<String>();
          tide.lowTimes.add(cols[3].text);
          tide.lowTimes.add(cols[7].text);
          tide.lowValues = List<double>();
          tide.lowValues.add(parseDouble(cols[4].text));
          tide.lowValues.add(parseDouble(cols[8].text));

          debugPrint(tag + ": row=" + tide.toJson().toString());
        }
      }
      debugPrint(tag + " result: " + data.toString());
      if (data.isNotEmpty) {
        return TideReportResponse(data);
      }*/
      return null;
    } catch (e) {
      Log.i(e);
    }
    throw ServerException(AssetStrings.getAppDataFailMessage);
  }
}
