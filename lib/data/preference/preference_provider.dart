import 'package:shared_preferences/shared_preferences.dart';

abstract class PreferenceProvider {
  SharedPreferences _pref;

  Future<SharedPreferences> getPref() async {
    if (_pref == null) {
      _pref = await SharedPreferences.getInstance();
    }
    return _pref;
  }
}
