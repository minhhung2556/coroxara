import 'package:coroxara/data/appdata/appdata_api.dart';
import 'package:coroxara/data/appdata/appdata_model.dart';
import 'package:coroxara/data/appdata/appdata_pref.dart';
import 'package:coroxara/data/base/repository.dart';
import 'package:coroxara/data/base/result.dart';
import 'package:coroxara/res/strings.dart';

class AppDataRepository extends Repository {
  AppDataResponse _appDataResponse;
  final AppDataApiProvider _api;
  final AppDataPrefProvider _pref;

  AppDataRepository(this._api, this._pref);

  @override
  void dispose() {
    //TODO
  }

  Future<Result<AppDataResponse>> getAppData() async {
    if (_appDataResponse != null)
      return Result<AppDataResponse>.success(_appDataResponse);

    _appDataResponse = await _pref.getAppData();
    if (_appDataResponse != null) {
      fetchFromServer();
      return Result<AppDataResponse>.success(_appDataResponse);
    } else
      return await fetchFromServer();
  }

  Future<dynamic> saveAppData(AppDataResponse res) async {
    _appDataResponse = res;
    return await _pref.saveAppData(res);
  }

  Future<Result<AppDataResponse>> fetchFromServer() async {
    try {
//      final start = DateTime.now();
      final res = await _api.getAppData();

//      final now = DateTime.now();
//      Log.d('fetchFromServer: ${now.difference(start).inSeconds}s');
      await saveAppData(res);
      return Result<AppDataResponse>.success(res);
    } catch (e) {
      print(e);
      return Result<AppDataResponse>.failure(
          AssetStrings.getAppDataFailMessage);
    }
  }
}
