import 'dart:convert' as json;

import 'package:coroxara/data/appdata/appdata_model.dart';
import 'package:coroxara/data/preference/preference_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppDataPrefProvider extends PreferenceProvider {
  static const keyAppDataResponse = "AppDataResponse";

  Future<dynamic> saveAppData(AppDataResponse res) async {
    try {
      final SharedPreferences pref = await getPref();
      final value = json.jsonEncode(res.toJson());
      return await pref.setString(keyAppDataResponse, value);
    } catch (e) {
      return false;
    }
  }

  Future<AppDataResponse> getAppData() async {
    try {
      final SharedPreferences pref = await getPref();
      final value = pref.getString(keyAppDataResponse);
      final map = value != null ? json.jsonDecode(value) : null;
      return AppDataResponse.fromJson(map);
    } catch (e) {
      return null;
    }
  }
}
