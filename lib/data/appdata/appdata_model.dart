import 'package:primitive_type_parser/primitive_type_parser.dart';

class DateCount {
  final DateTime date;
  final double count;

  DateCount({this.date, this.count});

  Map<String, dynamic> toJson() {
    return {
      'date': date == null ? null : date.toIso8601String(),
      'count': count == null ? null : count,
    };
  }

  factory DateCount.fromJson(Map<String, dynamic> json) {
    return DateCount(
      date: json['date'] == null ? null : DateTime.tryParse(json['date']),
      count: json['count'] == null ? null : parseDouble(json['count']),
    );
  }
}

class AppDataResponse {
  final double total;
  final double severeCondition;
  final double deaths;
  final double recovered;
  final List<DateCount> dateCases;
  final List<DateCount> dateDeathCases;
  final List<CountryCase> countryCases;
  final Prevention prevention;
  final Prognostic prognostic;
  final List<Hospital> hospitals;

  AppDataResponse({
    this.total,
    this.severeCondition,
    this.deaths,
    this.recovered,
    this.dateCases,
    this.countryCases,
    this.prevention,
    this.prognostic,
    this.hospitals,
    this.dateDeathCases,
  });

  Map<String, dynamic> toJson() {
    return {
      'total': total == null ? null : total,
      'severeCondition': severeCondition == null ? null : severeCondition,
      'deaths': deaths == null ? null : deaths,
      'recovered': recovered == null ? null : recovered,
      'dateCases': dateCases == null
          ? null
          : List<dynamic>.from(dateCases.map((x) => x.toJson())),
      'dateDeathCases': dateDeathCases == null
          ? null
          : List<dynamic>.from(dateDeathCases.map((x) => x.toJson())),
      'countryCases': countryCases == null
          ? null
          : List<dynamic>.from(countryCases.map((x) => x.toJson())),
      'prevention': prevention == null ? null : prevention.toJson(),
      'prognostic': prognostic == null ? null : prognostic.toJson(),
      'hospitals': hospitals == null
          ? null
          : List<dynamic>.from(hospitals.map((x) => x.toJson())),
    };
  }

  factory AppDataResponse.fromJson(Map<String, dynamic> json) {
    return AppDataResponse(
      total: json['total'] == null ? null : parseDouble(json['total']),
      severeCondition: json['severeCondition'] == null
          ? null
          : parseDouble(json['severeCondition']),
      deaths: json['deaths'] == null ? null : parseDouble(json['deaths']),
      recovered:
          json['recovered'] == null ? null : parseDouble(json['recovered']),
      dateCases: json['dateCases'] == null
          ? null
          : List<DateCount>.from(
              json['dateCases'].map((x) => DateCount.fromJson(x))),
      dateDeathCases: json['dateDeathCases'] == null
          ? null
          : List<DateCount>.from(
              json['dateDeathCases'].map((x) => DateCount.fromJson(x))),
      countryCases: json['countryCases'] == null
          ? null
          : List<CountryCase>.from(
              json['countryCases'].map((x) => CountryCase.fromJson(x))),
      prevention: json['prevention'] == null
          ? null
          : Prevention.fromJson(json['prevention']),
      prognostic: json['prognostic'] == null
          ? null
          : Prognostic.fromJson(json['prognostic']),
      hospitals: json['hospitals'] == null
          ? null
          : List<Hospital>.from(
              json['hospitals'].map((x) => Hospital.fromJson(x))),
    );
  }
}

class CountryCase {
  final String country;
  final double total;
  final double todayCases;
  final double deaths;
  final double todayDeaths;
  final double recovered;
  final double severe;
  final String region;

  CountryCase(
      {this.country,
      this.total,
      this.todayCases,
      this.deaths,
      this.todayDeaths,
      this.recovered,
      this.severe,
      this.region});

  Map<String, dynamic> toJson() {
    return {
      'country': country == null ? null : country,
      'total': total == null ? null : total,
      'todayCases': todayCases == null ? null : todayCases,
      'deaths': deaths == null ? null : deaths,
      'todayDeaths': todayDeaths == null ? null : todayDeaths,
      'recovered': recovered == null ? null : recovered,
      'severe': severe == null ? null : severe,
      'region': region == null ? null : region,
    };
  }

  factory CountryCase.fromJson(Map<String, dynamic> json) {
    return CountryCase(
      country: json['country'] == null ? null : parseString(json['country']),
      total: json['total'] == null ? null : parseDouble(json['total']),
      todayCases:
          json['todayCases'] == null ? null : parseDouble(json['todayCases']),
      deaths: json['deaths'] == null ? null : parseDouble(json['deaths']),
      todayDeaths:
          json['todayDeaths'] == null ? null : parseDouble(json['todayDeaths']),
      recovered:
          json['recovered'] == null ? null : parseDouble(json['recovered']),
      severe: json['severe'] == null ? null : parseDouble(json['severe']),
      region: json['region'] == null ? null : parseString(json['region']),
    );
  }
}

class PreventionItem {
  final String text;
  final String img;
  final String clip;

  PreventionItem({this.text, this.img, this.clip});

  Map<String, dynamic> toJson() {
    return {
      'text': text == null ? null : text,
      'img': img == null ? null : img,
      'clip': clip == null ? null : clip,
    };
  }

  factory PreventionItem.fromJson(Map<String, dynamic> json) {
    return PreventionItem(
      text: json['text'] == null ? null : parseString(json['text']),
      img: json['img'] == null ? null : parseString(json['img']),
      clip: json['clip'] == null ? null : parseString(json['clip']),
    );
  }
}

class Prevention {
  final String title;
  final List<PreventionItem> items;

  Prevention({this.title, this.items});

  Map<String, dynamic> toJson() {
    return {
      'title': title == null ? null : title,
      'items': items == null
          ? null
          : List<dynamic>.from(items.map((x) => x.toJson())),
    };
  }

  factory Prevention.fromJson(Map<String, dynamic> json) {
    return Prevention(
      title: json['title'] == null ? null : parseString(json['title']),
      items: json['items'] == null
          ? null
          : List<PreventionItem>.from(
              json['items'].map((x) => PreventionItem.fromJson(x))),
    );
  }
}

class PrognosticItem {
  final String title;
  final List<String> items;
  final String img;

  PrognosticItem({this.title, this.items, this.img});

  Map<String, dynamic> toJson() {
    return {
      'title': title == null ? null : title,
      'items': items == null ? null : List<dynamic>.from(items.map((x) => x)),
      'img': img == null ? null : img,
    };
  }

  factory PrognosticItem.fromJson(Map<String, dynamic> json) {
    return PrognosticItem(
      title: json['title'] == null ? null : parseString(json['title']),
      items: json['items'] == null
          ? null
          : List<String>.from(json['items'].map((x) => parseString(x))),
      img: json['img'] == null ? null : parseString(json['img']),
    );
  }
}

class Prognostic {
  final String title;
  final List<PrognosticItem> items;

  Prognostic({this.title, this.items});

  Map<String, dynamic> toJson() {
    return {
      'title': title == null ? null : title,
      'items': items == null
          ? null
          : List<dynamic>.from(items.map((x) => x.toJson())),
    };
  }

  factory Prognostic.fromJson(Map<String, dynamic> json) {
    return Prognostic(
      title: json['title'] == null ? null : parseString(json['title']),
      items: json['items'] == null
          ? null
          : List<PrognosticItem>.from(
              json['items'].map((x) => PrognosticItem.fromJson(x))),
    );
  }
}

class Hospital {
  final String province;
  final String name;
  final String info;
  final String address;
  final String mapUrl;
  final String workingHours;
  final List<String> phones;
  final String email;
  final String website;

  Hospital(
      {this.province,
      this.name,
      this.info,
      this.address,
      this.mapUrl,
      this.workingHours,
      this.phones,
      this.email,
      this.website});

  Map<String, dynamic> toJson() {
    return {
      'province': province == null ? null : province,
      'name': name == null ? null : name,
      'info': info == null ? null : info,
      'address': address == null ? null : address,
      'mapUrl': mapUrl == null ? null : mapUrl,
      'workingHours': workingHours == null ? null : workingHours,
      'phones':
          phones == null ? null : List<dynamic>.from(phones.map((x) => x)),
      'email': email == null ? null : email,
      'website': website == null ? null : website,
    };
  }

  factory Hospital.fromJson(Map<String, dynamic> json) {
    return Hospital(
      province: json['province'] == null ? null : parseString(json['province']),
      name: json['name'] == null ? null : parseString(json['name']),
      info: json['info'] == null ? null : parseString(json['info']),
      address: json['address'] == null ? null : parseString(json['address']),
      mapUrl: json['mapUrl'] == null ? null : parseString(json['mapUrl']),
      workingHours: json['workingHours'] == null
          ? null
          : parseString(json['workingHours']),
      phones: json['phones'] == null
          ? null
          : List<String>.from(json['phones'].map((x) => parseString(x))),
      email: json['email'] == null ? null : parseString(json['email']),
      website: json['website'] == null ? null : parseString(json['website']),
    );
  }
}
