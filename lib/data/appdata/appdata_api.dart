import 'package:coroxara/data/appdata/appdata_model.dart';
import 'package:coroxara/data/base/exception.dart';
import 'package:coroxara/data/constants.dart';
import 'package:coroxara/data/network/api_provider.dart';
import 'package:coroxara/helper/logger.dart';
import 'package:coroxara/helper/string_utils.dart';
import 'package:coroxara/res/strings.dart';
import 'package:http/http.dart';
import 'package:spreadsheet_decoder/spreadsheet_decoder.dart';

class AppDataApiProvider extends ApiProvider {
  AppDataApiProvider(String url) : super(url);

  Future<AppDataResponse> getAppData() async {
    final response = await get(DATA_URL);
    final bytes = response?.bodyBytes;
    final decoder =
        bytes != null ? SpreadsheetDecoder.decodeBytes(bytes) : null;
    if (decoder != null) {
      final sheet1 = decoder.tables.keys.first;
      SpreadsheetTable table = decoder.tables[sheet1];

      double total = double.tryParse(table.rows[0][1]?.toString() ?? "0");
      double severeCondition =
          double.tryParse(table.rows[1][1]?.toString() ?? "0");
      double deaths = double.tryParse(table.rows[2][1]?.toString() ?? "0");
      double recovered = double.tryParse(table.rows[3][1]?.toString() ?? "0");
      DateTime fromDate = dateTimeFromString(table.rows[4][0]);
      DateTime toDate = dateTimeFromString(table.rows[4][1]);
      List<double> cases = table.rows[5][0]
          .toString()
          .replaceAll(new RegExp(r"\s+\b|\b\s"), "")
          .split(',')
          .map((i) => double.tryParse(i))
          .toList();
      List<double> deathCases = table.rows[5][1]
          .toString()
          .replaceAll(new RegExp(r"\s+\b|\b\s"), "")
          .split(',')
          .map((i) => double.tryParse(i))
          .toList();
      final dateCases = List<DateCount>();
      final dateDeathCases = List<DateCount>();
      final Duration range = toDate.difference(fromDate);
      for (var d = 0; d <= range.inDays; d++) {
        var date = fromDate.add(Duration(days: d));
        dateCases.add(DateCount(
            date: date, count: d < cases.length ? cases[d] : cases.last));
        dateDeathCases.add(DateCount(
            date: date,
            count: d < deathCases.length ? deathCases[d] : deathCases.last));
      }

      List<CountryCase> countryCases = List<CountryCase>();

      for (var r = 1; r < table.rows.length; r++) {
        var o = CountryCase(
          country: table.rows[r][3] ?? "",
          total: double.tryParse(table.rows[r][4]?.toString() ?? "0"),
          todayCases: double.tryParse(table.rows[r][5]?.toString() ?? "0"),
          deaths: double.tryParse(table.rows[r][6]?.toString() ?? "0"),
          todayDeaths: double.tryParse(table.rows[r][7]?.toString() ?? "0"),
          recovered: double.tryParse(table.rows[r][8]?.toString() ?? "0"),
          severe: double.tryParse(table.rows[r][9]?.toString() ?? "0"),
//          region: table.rows[r][10] ?? "",
        );
        countryCases.add(o);
      }

      final sheet2 = decoder.tables.keys.elementAt(1);
      table = decoder.tables[sheet2];
      final preventionTitle = table.rows[0][0];
      final preventionItems = List<PreventionItem>();
      for (var r = 1; r < table.maxRows; r++) {
        preventionItems.add(PreventionItem(
          text: table.rows[r][0],
          img: table.rows[r][1],
          clip: table.rows[r][2],
        ));
      }
      final prevention =
          Prevention(title: preventionTitle, items: preventionItems);

      final sheet3 = decoder.tables.keys.elementAt(2);
      table = decoder.tables[sheet3];
      final prognosticTitle = table.rows[0][0];
      final prognosticItems = List<PrognosticItem>();
      for (var r = 1; r < table.maxRows; r++) {
        prognosticItems.add(PrognosticItem(
          title: table.rows[r][0],
          items: table.rows[r][1].toString().split('\n'),
          img: table.rows[r][2],
        ));
      }
      final prognostic =
          Prognostic(title: prognosticTitle, items: prognosticItems);

      final sheet4 = decoder.tables.keys.elementAt(3);
      table = decoder.tables[sheet4];
      final hospitals = List<Hospital>();
      for (var r = 0; r < table.maxRows; r++) {
        hospitals.add(Hospital(
          province: table.rows[r][0],
          name: table.rows[r][1],
          info: table.rows[r][2],
          address: table.rows[r][3],
          mapUrl: table.rows[r][4],
          workingHours: table.rows[r][5],
          phones: table.rows[r][6].toString().split('\n'),
          email: table.rows[r][7],
          website: table.rows[r][8],
        ));
      }

      AppDataResponse res = AppDataResponse(
        total: total,
        severeCondition: severeCondition,
        deaths: deaths,
        recovered: recovered,
        dateCases: dateCases,
        dateDeathCases: dateDeathCases,
        countryCases: countryCases,
        prevention: prevention,
        prognostic: prognostic,
        hospitals: hospitals,
      );

      Log.d(res.toJson().toString());

      return res;
    }

    throw ServerException(AssetStrings.getAppDataFailMessage);
  }
}
