class AdConstants {
  static const AD_MOB_APP_ID_ANDROID = "ca-app-pub-2895493109779094~4660208586";
  static const AD_BANNER_ID_ANDROID = "ca-app-pub-2895493109779094/6105152190";
  static const AD_MOB_APP_ID_IOS = "ca-app-pub-2895493109779094~8945570978";
  static const AD_BANNER_ID_IOS = "ca-app-pub-2895493109779094/3700434417";
}
