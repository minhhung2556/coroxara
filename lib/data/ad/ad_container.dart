import 'package:firebase_admob/firebase_admob.dart';

mixin BannerAdContainer {
  BannerAd myBanner;

  String get bannerId;

  void initAd() {
    myBanner = BannerAd(
      // Replace the testAdUnitId with an ad unit id from the AdMob dash.
      // https://developers.google.com/admob/android/test-ads
      // https://developers.google.com/admob/ios/test-ads
      adUnitId: bannerId,
      size: AdSize.banner,
      listener: (MobileAdEvent event) {
        print("BannerAd event is $event");
        onBannerLoaded(AdSize.banner.height.toDouble());
      },
    );
  }

  void showAd() {
    myBanner
      // typically this happens well before the ad is shown
      ..load()
      ..show(
        // Positions the banner ad 60 pixels from the bottom of the screen
        // Positions the banner ad 10 pixels from the center of the screen to the right
        // Banner Position
        anchorType: AnchorType.bottom,
      );
  }

  void onBannerLoaded(double height);
}

mixin FullscreenAdContainer {
  InterstitialAd myInterstitial;

  String get interstitialId;

  void initAd() async {
    myInterstitial = InterstitialAd(
      // Replace the testAdUnitId with an ad unit id from the AdMob dash.
      // https://developers.google.com/admob/android/test-ads
      // https://developers.google.com/admob/ios/test-ads
      adUnitId: interstitialId,
      listener: (MobileAdEvent event) {
        print("InterstitialAd event is $event");
      },
    );

    // typically this happens well before the ad is shown
    await myInterstitial.load();
  }

  void showAd() async {
    await myInterstitial.show(
      // Positions the banner ad 60 pixels from the bottom of the screen
      // Positions the banner ad 10 pixels from the center of the screen to the right
      // Banner Position
      anchorType: AnchorType.bottom,
    );
    await myInterstitial.dispose();
    await myInterstitial.load();
  }
}
