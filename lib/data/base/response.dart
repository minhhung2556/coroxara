
class Response {

  int hello;

	Response.fromJsonMap(Map<String, dynamic> map): 
		hello = map["hello"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['hello'] = hello;
		return data;
	}
}
