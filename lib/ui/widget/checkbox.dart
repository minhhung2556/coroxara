import 'package:flutter/material.dart';

class CustomCheckbox extends StatelessWidget {
  final bool selected;

  const CustomCheckbox({Key key, this.selected}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      height: 22,
      width: 22,
      child: selected
          ? Icon(
              Icons.check,
              color: Colors.white,
              size: 16,
            )
          : null,
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: selected ? Theme.of(context).accentColor : Colors.transparent,
          shape: BoxShape.circle,
          border: selected
              ? null
              : Border.all(color: Theme.of(context).accentColor, width: 2)),
      duration: Duration(milliseconds: 160),
    );
  }
}
