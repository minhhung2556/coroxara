import 'package:coroxara/res/styles.dart';
import 'package:flutter/material.dart';

class TabTitle extends StatelessWidget {
  final String text;
  final EdgeInsets margin;

  const TabTitle({Key key, this.text, this.margin}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.scaleDown,
      child: Container(
        margin: margin
            ?.add(EdgeInsets.only(top: MediaQuery.of(context).padding.top)),
        child: Text(
          text,
          textAlign: TextAlign.center,
          style: AssetStyles.boldBlack16,
        ),
      ),
    );
  }
}
