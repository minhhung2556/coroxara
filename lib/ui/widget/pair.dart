class Pair<M, N> {
  final M first;
  final N second;

  Pair(this.first, this.second);

  Map<String, dynamic> toMap() {
    return {
      'first': this.first,
      'second': this.second,
    };
  }
}
