import 'package:coroxara/helper/string_utils.dart';
import 'package:coroxara/res/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:rxdart/rxdart.dart';

class SearchInAppBar extends StatefulWidget {
  final bool visible;
  final Function(String) onSubmitted;
  final double leftMargin;
  final double rightMargin;
  final Function() onClear;
  final String text;

  const SearchInAppBar({
    Key key,
    this.visible = false,
    this.onSubmitted,
    this.leftMargin = kToolbarHeight,
    this.rightMargin = 14,
    this.onClear,
    this.text,
  }) : super(key: key);

  @override
  _SearchInAppBarState createState() => _SearchInAppBarState();
}

class _SearchInAppBarState extends State<SearchInAppBar> {
  final clearVisible = BehaviorSubject<bool>();
  final textEditingController = TextEditingController();

  @override
  void initState() {
    textEditingController.addListener(() {
      clearVisible.add(isNotEmpty(textEditingController.text));
    });
    super.initState();
  }

  @override
  void dispose() {
    clearVisible.close();
    super.dispose();
  }

  @override
  void didUpdateWidget(SearchInAppBar oldWidget) {
    if (oldWidget.text != widget.text) {
      textEditingController.text = widget.text;
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    if (!widget.visible) return Container();

    return Container(
      margin: EdgeInsets.only(
        top: MediaQuery.of(context).padding.top + 8,
//        left: widget.leftMargin,
//        right: widget.rightMargin,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Theme.of(context).primaryColor,
      ),
      child: Stack(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(8).add(EdgeInsets.only(right: 4)),
                child: const SearchIcon(),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(right: 8),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: TextField(
                          controller: textEditingController,
                          autofocus: true,
                          obscureText: false,
                          autocorrect: false,
                          style: AssetStyles.regularWhite16
                              .copyWith(color: Colors.white.withOpacity(0.8)),
                          maxLines: 1,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "Tìm kiếm",
                            hintStyle: AssetStyles.regularWhite16
                                .copyWith(color: Colors.white.withOpacity(0.8)),
                          ),
                          onSubmitted: widget.onSubmitted,
                        ),
                      ),
                      StreamBuilder<bool>(
                        builder: (context, snapshot) {
                          return snapshot.data == true
                              ? IconButton(
                                  icon: Icon(
                                    Icons.clear,
                                    color: Colors.white,
                                    size: 24,
                                  ),
                                  onPressed: () {
                                    textEditingController.clear();
                                    textEditingController.clearComposing();
                                    if (widget.onClear != null)
                                      widget.onClear();
                                  },
                                )
                              : Container(width: 17, height: 17);
                        },
                        stream: clearVisible.stream,
                        initialData: false,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

class SearchIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const Icon(
      Icons.search,
      color: Colors.white,
      size: 24,
    );
  }

  const SearchIcon();
}
