import 'dart:io';

import 'package:coroxara/data/ad/ad_container.dart';
import 'package:coroxara/data/ad/constants.dart';
import 'package:coroxara/data/appdata/appdata_repository.dart';
import 'package:coroxara/res/colors.dart';
import 'package:coroxara/ui/home/home_bloc.dart';
import 'package:cuberto_bottom_bar/cuberto_bottom_bar.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:lightweight_bloc/lightweight_bloc.dart';

import 'hospital/hospital_tab.dart';
import 'overview/over_view_tab.dart';
import 'prevention/prevention_tab.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();

  static Widget newInstance() => BlocProvider(
        builder: (context) => HomeBloc(GetIt.I.get<AppDataRepository>()),
        child: HomeScreen(),
      );
}

class _HomeScreenState extends State<HomeScreen> with BannerAdContainer {
  final _pageViewController = PageController();
  double adPadding = 0;

  @override
  void initState() {
    initAd();
    showAd();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<HomeBloc, HomeState>(
      listener: (_, __, state) {
        if (state.currentTabIndex != _pageViewController.page) {
          if (_pageViewController.hasClients) {
            try {
              _pageViewController.animateToPage(state.currentTabIndex,
                  duration: Duration(milliseconds: 200),
                  curve: Curves.decelerate);
            } catch (e) {}
          }
        }
      },
      child: BlocListener<HomeBloc, HomeState>(
        listener: (_, __, state) {},
        child: BlocWidgetBuilder<HomeBloc, HomeState>(
          builder: (context, bloc, state) {
            return Container(
              margin: EdgeInsets.only(bottom: adPadding.toDouble()),
              child: Scaffold(
                backgroundColor: AssetColors.homeBackground,
                body: Padding(
                  padding:
                      EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                  child: Stack(
                    fit: StackFit.expand,
                    children: <Widget>[
                      PageView(
                        controller: _pageViewController,
                        physics: NeverScrollableScrollPhysics(),
                        children: state.tabs.map(
                          (tab) {
                            switch (tab.id) {
                              case TabItem.OVERVIEW:
                                return OverViewTab.newInstance(
                                    bloc.appDataRepository);
                              case TabItem.INFORMATION:
                                return PreventionTab.newInstance(state.appData);
                              case TabItem.HOSPITAL:
                                return HospitalTab.newInstance(state.appData);
                              default:
                                return Container();
                            }
                          },
                        ).toList(),
                      ),
                    ],
                  ),
                ),
                bottomNavigationBar: CubertoBottomBar(
                  textColor: AssetColors.textGrey,
                  barBackgroundColor: AssetColors.primaryColor,
                  inactiveIconColor: AssetColors.textGrey,
                  tabColor: AssetColors.textGrey,
                  tabStyle: CubertoTabStyle.STYLE_FADED_BACKGROUND,
                  selectedTab: state.currentTabIndex,
                  barShadow: [BoxShadow(blurRadius: 16, spreadRadius: 1)],
                  padding: EdgeInsets.symmetric(vertical: 16),
                  tabs: state.tabs
                      .map(
                        (item) => TabData(
                          iconData: item.icon,
                          title: item.title,
                        ),
                      )
                      .toList(),
                  onTabChangedListener:
                      (int position, String title, Color tabColor) {
                    bloc.onUserChangeIndex(position);
                  },
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  @override
  String get bannerId => kReleaseMode
      ? (Platform.isAndroid
          ? AdConstants.AD_BANNER_ID_ANDROID
          : AdConstants.AD_BANNER_ID_IOS)
      : BannerAd.testAdUnitId;

  @override
  void onBannerLoaded(double height) {
    setState(() {
      adPadding = height;
    });
  }
}
