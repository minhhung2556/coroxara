import 'package:coroxara/data/appdata/appdata_model.dart';
import 'package:coroxara/res/colors.dart';
import 'package:coroxara/res/images.dart';
import 'package:coroxara/res/styles.dart';
import 'package:coroxara/ui/home/home_common_widgets.dart';
import 'package:coroxara/ui/home/overview/over_view_tab.dart';
import 'package:coroxara/ui/widget/loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class PrognosticTab extends StatelessWidget {
  const PrognosticTab({Key key, this.appData}) : super(key: key);

  static Widget newInstance(AppDataResponse appData) =>
      PrognosticTab(appData: appData);

  final AppDataResponse appData;

  @override
  Widget build(BuildContext context) {
    final children = <Widget>[
      BaseTabTitle(title: 'Triệu chứng'),
    ];
    if (appData != null) {
      children.addAll(appData.prognostic.items.map((item) => Padding(
            padding: const EdgeInsets.only(
                left: screenPadding,
                right: screenPadding,
                bottom: screenPadding),
            child: ClipRRect(
              clipBehavior: Clip.antiAliasWithSaveLayer,
              borderRadius: BorderRadius.circular(8),
              child: Container(
                color: AssetColors.primaryColor,
                child: Column(
                  children: <Widget>[
                    AspectRatio(
                      aspectRatio: 16 / 9,
                      child: Image.network(
                        item.img,
                        fit: BoxFit.cover,
                        loadingBuilder: (context, image, progress) =>
                            progress == null
                                ? image
                                : Image.asset(AssetImages.placeHolder,
                                    fit: BoxFit.cover),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 12, vertical: 16),
                      child: Column(
                        children: item.items
                            .map((itemChild) => Padding(
                                  padding: const EdgeInsets.only(bottom: 12),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      const ColorDot(
                                          color: AssetColors.accentColor),
                                      const SizedBox(width: 8),
                                      Expanded(
                                        child: Text(
                                          itemChild,
                                          style: AssetStyles.mediumGrey14,
                                        ),
                                      ),
                                    ],
                                  ),
                                ))
                            .toList(),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )));
    } else {
      children.add(
          Container(height: 400, child: Center(child: LoadingIndicator())));
    }
    return BaseTab(
      child: Column(
        children: children,
      ),
    );
  }
}
