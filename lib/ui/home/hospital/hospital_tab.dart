import 'package:coroxara/data/appdata/appdata_model.dart';
import 'package:coroxara/res/colors.dart';
import 'package:coroxara/res/strings.dart';
import 'package:coroxara/res/styles.dart';
import 'package:coroxara/ui/home/home_common_widgets.dart';
import 'package:coroxara/ui/home/overview/over_view_tab.dart';
import 'package:coroxara/ui/widget/loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:url_launcher/url_launcher.dart' as launcher;

class HospitalTab extends StatelessWidget {
  const HospitalTab({Key key, this.appData}) : super(key: key);

  static Widget newInstance(AppDataResponse appData) =>
      HospitalTab(appData: appData);

  final AppDataResponse appData;

  @override
  Widget build(BuildContext context) {
    final children = <Widget>[
      BaseTabTitle(title: AssetStrings.homeTabScreenTitles[2]),
    ];
    if (appData != null) {
      children.addAll(appData.hospitals.map((item) => Padding(
            padding: const EdgeInsets.only(
                left: screenPadding,
                right: screenPadding,
                bottom: screenPadding),
            child: Container(
              decoration: BoxDecoration(
                color: AssetColors.primaryColor,
                borderRadius: BorderRadius.circular(8),
              ),
              padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      const ColorDot(color: AssetColors.accentColor),
                      const SizedBox(width: 8),
                      Expanded(
                        child: Text(
                          item.name,
                          style: AssetStyles.boldGrey14,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 8),
                  Padding(
                    padding: const EdgeInsets.only(left: 16),
                    child: Text(
                      item.address ?? "",
                      style: AssetStyles.mediumGrey14,
                      textAlign: TextAlign.left,
                    ),
                  ),
                  const SizedBox(height: 8),
                  Padding(
                    padding: const EdgeInsets.only(left: 16),
                    child: Text(
                      item.workingHours ?? AssetStrings.unknown,
                      style: AssetStyles.mediumGrey14.apply(
                          color: item.workingHours != null
                              ? AssetColors.textGrey
                              : AssetColors.textDarkGrey),
                      textAlign: TextAlign.left,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          item.province,
                          style: AssetStyles.boldGrey12,
                          textAlign: TextAlign.left,
                        ),
                      ),
                      if (item.phones != null)
                        IconButton(
                          icon: Icon(
                            Icons.call,
                            color: AssetColors.textGrey,
                          ),
                          onPressed: () {
                            showPickerModalPhones(context, item.phones);
                          },
                        ),
                      if (item.email != null)
                        IconButton(
                          icon: Icon(
                            Icons.email,
                            color: AssetColors.textGrey,
                          ),
                          onPressed: () async {
                            await launcher.launch('mailto:${item.email}');
                          },
                        ),
                      if (item.website != null)
                        IconButton(
                          icon: Icon(
                            Icons.open_in_browser,
                            color: AssetColors.textGrey,
                          ),
                          onPressed: () async {
                            await launcher.launch('${item.website}');
                          },
                        ),
                      if (item.mapUrl != null)
                        IconButton(
                          icon: Icon(
                            Icons.directions,
                            color: AssetColors.textGrey,
                          ),
                          onPressed: () async {
                            await launcher.launch('${item.mapUrl}');
                          },
                        ),
                    ],
                  ),
                ],
              ),
            ),
          )));
    } else {
      children.add(
          Container(height: 400, child: Center(child: LoadingIndicator())));
    }
    return BaseTab(
      child: Column(
        children: children,
      ),
    );
  }

  showPickerModalPhones(BuildContext context, List<String> items) {
    if (items == null || items.isEmpty) return false;
    if (items.length == 1)
      launcher.launch('tel:${items.first}');
    else {
      new Picker(
          adapter: PickerDataAdapter<String>(pickerdata: items),
          changeToFirst: true,
          hideHeader: false,
          cancelText: 'Quay lại',
          confirmText: 'Gọi',
          footer: SizedBox(height: 100),
          cancelTextStyle: AssetStyles.regularPrimaryColor16,
          confirmTextStyle: AssetStyles.regularPrimaryColor16,
          onConfirm: (Picker picker, List value) async {
            print(value.toString());
            print(picker.adapter.text);
            launcher.launch('tel:${picker.adapter.text}');
          }).showModal<String>(context);
    }
  }
}
