import 'package:coroxara/res/styles.dart';
import 'package:flutter/material.dart';

class BaseTab extends StatefulWidget {
  final Widget child;

  const BaseTab({Key key, this.child}) : super(key: key);

  @override
  _BaseTabState createState() => _BaseTabState();
}

class _BaseTabState extends State<BaseTab> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: widget.child,
    );
  }
}

const screenPadding = 20.0;

class BaseTabTitle extends StatelessWidget {
  final String title;

  const BaseTabTitle({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(screenPadding),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              title,
              style: AssetStyles.homeTabScreenTitle,
            ),
          ),
        ),
        const SizedBox(height: screenPadding),
      ],
    );
  }
}
