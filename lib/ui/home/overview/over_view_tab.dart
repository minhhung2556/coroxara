import 'package:coroxara/data/appdata/appdata_model.dart';
import 'package:coroxara/data/appdata/appdata_repository.dart';
import 'package:coroxara/res/colors.dart';
import 'package:coroxara/res/strings.dart';
import 'package:coroxara/res/styles.dart';
import 'package:coroxara/ui/home/home_common_widgets.dart';
import 'package:coroxara/ui/widget/loading.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:lightweight_bloc/lightweight_bloc.dart';

import 'over_view_bloc.dart';

class OverViewTab extends StatefulWidget {
  static Widget newInstance(AppDataRepository appDataRepository) =>
      BlocProvider(
        builder: (context) => OverViewBloc(appDataRepository),
        child: OverViewTab(),
      );

  OverViewTab({Key key}) : super(key: key);

  @override
  _OverViewTabState createState() => _OverViewTabState();
}

class _OverViewTabState extends State<OverViewTab> {
  @override
  Widget build(BuildContext context) {
    return BaseTab(
      child: BlocWidgetBuilder<OverViewBloc, OverViewState>(
          builder: (context, bloc, state) {
        final children = <Widget>[
          BaseTabTitle(title: AssetStrings.homeTabScreenTitles[0]),
        ];
        if (state.id == OverViewStateId.DoneLoading) {
          children.addAll([
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: screenPadding),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                    child: OverviewItem(
                      color: AssetColors.overviewItemColors[0],
                      name: AssetStrings.overviewItemNames[0],
                      count: state.appData?.deaths?.toInt() ?? 0,
                    ),
                  ),
                  const SizedBox(width: 8),
                  Expanded(
                    flex: 2,
                    child: OverviewItem(
                      color: AssetColors.overviewItemColors[1],
                      name: AssetStrings.overviewItemNames[1],
                      count: state.appData?.total?.toInt() ?? 0,
                    ),
                  ),
                  const SizedBox(width: 8),
                  Expanded(
                    child: OverviewItem(
                      color: AssetColors.overviewItemColors[2],
                      name: AssetStrings.overviewItemNames[2],
                      count: state.appData?.recovered?.toInt() ?? 0,
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: screenPadding),
            Padding(
              padding: const EdgeInsets.only(right: screenPadding * 2),
              child: Chart(
                bloc: bloc,
              ),
            ),
            const SizedBox(height: screenPadding),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: screenPadding),
              child: CountryCases(data: bloc.latestState.appData.countryCases),
            ),
          ]);
        } else {
          children.add(
              Container(height: 400, child: Center(child: LoadingIndicator())));
        }
        return Column(
          children: children,
        );
      }),
    );
  }
}

class OverviewItem extends StatelessWidget {
  final Color color;
  final String name;
  final int count;

  const OverviewItem({Key key, this.color, this.name, this.count})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(15)),
        color: color,
      ),
      child: FittedBox(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              name,
              style: AssetStyles.mediumGrey14,
            ),
            const SizedBox(height: 8),
            Text(
              '$count',
              style: AssetStyles.boldGrey24,
            ),
          ],
        ),
      ),
    );
  }
}

class Chart extends StatefulWidget {
  final OverViewBloc bloc;

  const Chart({Key key, this.bloc}) : super(key: key);

  @override
  _ChartState createState() => _ChartState();
}

class _ChartState extends State<Chart> {
  static final DateFormat _dateFormat = DateFormat('dd/MM/yyyy');
  static final double _valueStep = 1000;
  List<List<DateCount>> rawDatas, sortedRawDatas;

  @override
  void initState() {
    final appData = widget.bloc.latestState.appData;
    rawDatas = [appData.dateDeathCases, appData.dateCases];
    sortedRawDatas = [
      List.from(appData.dateDeathCases),
      List.from(appData.dateCases)
    ];
    sortedRawDatas
        .forEach((list) => list.sort((a, b) => a.count > b.count ? 1 : -1));
    super.initState();
  }

  LineChartData getChartLayoutData() {
    final lineBarsData = getChartData(rawDatas);
    final double maxX = rawDatas[1].length.toDouble();
    final double maxY = (rawDatas[1].last.count ~/ _valueStep).toDouble();
//    Log.d('getChartLayoutData: maxX=$maxX');
    return LineChartData(
      lineTouchData: LineTouchData(
        touchTooltipData: LineTouchTooltipData(
          tooltipBgColor: AssetColors.textGrey,
          getTooltipItems: (spots) => spots
              .map((s) => LineTooltipItem(
                  (s.y * _valueStep).toInt().toString(),
                  AssetStyles.regularBlack14.apply(
                      color: AssetColors.overviewItemColors[spots.indexOf(s)])))
              .toList(),
        ),
        handleBuiltInTouches: true,
      ),
      gridData: const FlGridData(
        show: false,
      ),
      titlesData: FlTitlesData(
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          textStyle: AssetStyles.boldGrey12,
          margin: 10,
          getTitles: (value) {
//            Log.d('getTitles: $value');
            if (value == 0) {
              return _dateFormat
                  .format(widget.bloc.latestState.appData.dateCases.first.date);
            } else if (value.toInt() == maxX ~/ 2) {
              return _dateFormat.format(widget
                  .bloc.latestState.appData.dateCases[value.toInt()].date);
            } else if (value == maxX) {
              return _dateFormat
                  .format(widget.bloc.latestState.appData.dateCases.last.date);
            }
            return '';
          },
        ),
        leftTitles: SideTitles(
          showTitles: true,
          textStyle: AssetStyles.boldGrey12,
          getTitles: (value) {
            if (value == 1 || value == maxY ~/ 2 || value == maxY)
              return '${value.toInt()}K';
            return '';
          },
          margin: 8,
          reservedSize: 30,
        ),
      ),
      borderData: FlBorderData(
        show: true,
        border: Border(
          bottom: BorderSide(
            color: AssetColors.textGrey,
            width: 1,
          ),
          left: BorderSide(
            color: Colors.transparent,
          ),
          right: BorderSide(
            color: Colors.transparent,
          ),
          top: BorderSide(
            color: Colors.transparent,
          ),
        ),
      ),
      minX: 0,
      maxX: maxX,
      maxY: maxY,
      minY: 0,
      lineBarsData: lineBarsData,
    );
  }

  List<LineChartBarData> getChartData(List<List<DateCount>> rawDatas) {
    return rawDatas
        .map((rd) => LineChartBarData(
              spots: rd
                  .map((dateCase) => FlSpot(rd.indexOf(dateCase).toDouble(),
                      dateCase.count / _valueStep))
                  .toList(),
              isCurved: true,
              colors: [
                AssetColors.overviewItemColors[rawDatas.indexOf(rd)],
              ],
              barWidth: 5,
              isStrokeCapRound: true,
              dotData: FlDotData(
                show: false,
              ),
              belowBarData: BarAreaData(
                show: false,
              ),
            ))
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 16 / 9,
      child: LineChart(
        getChartLayoutData(),
        swapAnimationDuration: Duration(milliseconds: 250),
      ),
    );
  }
}

class ColorDot extends StatelessWidget {
  final Color color;

  const ColorDot({Key key, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 5,
      height: 5,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: color,
      ),
    );
  }
}

class CountryCases extends StatelessWidget {
  final List<CountryCase> data;
  static const paddingBottom = EdgeInsets.only(bottom: 16);
  const CountryCases({
    Key key,
    this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: paddingBottom,
      child: Table(
        defaultColumnWidth: IntrinsicColumnWidth(),
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        columnWidths: {0: IntrinsicColumnWidth(flex: 2)},
        children: data
            .map((cc) => TableRow(
                  children: <Widget>[
                    Padding(
                      padding: paddingBottom,
                      child: Text(
                        '${cc.country}',
                        style: AssetStyles.boldGrey12,
                      ),
                    ),
                    const SizedBox(width: 16),
                    Padding(
                      padding: paddingBottom,
                      child: ColorDot(color: AssetColors.overviewItemColors[0]),
                    ),
                    const SizedBox(width: 6),
                    Padding(
                      padding: paddingBottom,
                      child: Text(
                        '${cc.deaths.toInt()}',
                        style: AssetStyles.mediumGrey14,
                        textAlign: TextAlign.left,
                      ),
                    ),
                    const SizedBox(width: 16),
                    Padding(
                      padding: paddingBottom,
                      child: ColorDot(color: AssetColors.overviewItemColors[1]),
                    ),
                    const SizedBox(width: 6),
                    Padding(
                      padding: paddingBottom,
                      child: Text(
                        '${cc.total.toInt()}',
                        style: AssetStyles.mediumGrey14,
                        textAlign: TextAlign.left,
                      ),
                    ),
                    const SizedBox(width: 16),
                    Padding(
                      padding: paddingBottom,
                      child: ColorDot(color: AssetColors.overviewItemColors[2]),
                    ),
                    const SizedBox(width: 6),
                    Padding(
                      padding: paddingBottom,
                      child: Text(
                        '${cc.recovered.toInt()}',
                        style: AssetStyles.mediumGrey14,
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ],
                ))
            .toList(),
      ),
    );
  }
}
