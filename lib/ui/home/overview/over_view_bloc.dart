import 'package:coroxara/data/appdata/appdata_model.dart';
import 'package:coroxara/data/appdata/appdata_repository.dart';
import 'package:lightweight_bloc/lightweight_bloc.dart';

class OverViewBloc extends Bloc<OverViewState> {
  AppDataRepository _appDataRepository;

  OverViewBloc(this._appDataRepository);

  @override
  void init() async {
    final appData = await _appDataRepository.getAppData();
    if (appData != null && appData.data != null) {
      update(latestState.copyWith(
          id: OverViewStateId.DoneLoading, appData: appData.data));
    } else {
      //TODO error state
    }
  }

  @override
  OverViewState get initialState => OverViewState(id: OverViewStateId.Loading);

  String getSevereRate() {
    return '${(((latestState.appData?.severeCondition ?? 0) / (latestState.appData?.total ?? 1)) * 100).floor()}%';
  }

  String getDeathRate() {
    return '${(((latestState.appData?.deaths ?? 0) / (latestState.appData?.total ?? 1)) * 100).floor()}%';
  }

  String getRecoveredRate() {
    return '${(((latestState.appData?.recovered ?? 0) / (latestState.appData?.total ?? 1)) * 100).floor()}%';
  }
}

class OverViewState {
  final OverViewStateId id;
  final AppDataResponse appData;

  OverViewState({
    this.id,
    this.appData,
  });

  OverViewState copyWith({
    OverViewStateId id,
    AppDataResponse appData,
  }) {
    return OverViewState(
      id: id ?? this.id,
      appData: appData ?? this.appData,
    );
  }
}

enum OverViewStateId { Loading, DoneLoading }
