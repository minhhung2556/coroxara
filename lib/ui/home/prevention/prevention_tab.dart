import 'package:coroxara/data/appdata/appdata_model.dart';
import 'package:coroxara/res/colors.dart';
import 'package:coroxara/res/images.dart';
import 'package:coroxara/res/styles.dart';
import 'package:coroxara/ui/home/home_common_widgets.dart';
import 'package:coroxara/ui/home/prognostic/prognostic_tab.dart';
import 'package:coroxara/ui/widget/loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class PreventionTab extends StatelessWidget {
  const PreventionTab({Key key, this.appData}) : super(key: key);

  static Widget newInstance(AppDataResponse appData) =>
      PreventionTab(appData: appData);

  final AppDataResponse appData;

  @override
  Widget build(BuildContext context) {
    final children = <Widget>[
      BaseTabTitle(title: 'Phòng ngừa'),
    ];
    if (appData != null) {
      children.add(Container(
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: appData.prevention.items
                .map((item) => Container(
                      width: (MediaQuery.of(context).size.width -
                              screenPadding * 2) *
                          0.8,
                      padding: const EdgeInsets.only(
                          left: screenPadding,
                          right: screenPadding,
                          bottom: screenPadding),
                      child: ClipRRect(
                        clipBehavior: Clip.antiAliasWithSaveLayer,
                        borderRadius: BorderRadius.circular(8),
                        child: Container(
                          color: AssetColors.primaryColor,
                          child: Column(
                            children: <Widget>[
                              AspectRatio(
                                aspectRatio: 16 / 9,
                                child: Image.network(
                                  item.img,
                                  fit: BoxFit.cover,
                                  loadingBuilder: (context, image, progress) =>
                                      progress == null
                                          ? image
                                          : Image.asset(AssetImages.placeHolder,
                                              fit: BoxFit.cover),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 12, vertical: 16),
                                child: Text(
                                  item.text,
                                  textAlign: TextAlign.left,
                                  style: AssetStyles.mediumGrey14,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ))
                .toList(),
          ),
        ),
      ));
      children.add(PrognosticTab.newInstance(appData));
    } else {
      children.add(
          Container(height: 400, child: Center(child: LoadingIndicator())));
    }
    return BaseTab(
      child: Column(
        children: children,
      ),
    );
  }
}
