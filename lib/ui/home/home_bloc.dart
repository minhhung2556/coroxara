import 'package:coroxara/data/appdata/appdata_model.dart';
import 'package:coroxara/data/appdata/appdata_repository.dart';
import 'package:coroxara/res/images.dart';
import 'package:coroxara/res/strings.dart';
import 'package:flutter/material.dart';
import 'package:lightweight_bloc/lightweight_bloc.dart';

class HomeBloc extends Bloc<HomeState> {
  AppDataRepository appDataRepository;

  HomeBloc(this.appDataRepository);

  @override
  void init() async {
//    checkPermissions();
//    await setupAd();
    final appData = await appDataRepository.getAppData();
    if (appData.isSuccessful) {
      update(latestState.copyWith(
          id: HomeStateId.DoneLoading, appData: appData.data));
    }
  }

//  Future<void> setupAd() async {
//    await FirebaseAdMob.instance.initialize(appId: AdConstants.AD_MOB_APP_ID);
//  }

  @override
  HomeState get initialState =>
      HomeState(id: HomeStateId.Loading, currentTabIndex: 0);

  void onUserChangeIndex(int newIndex) {
    update(latestState.copyWith(currentTabIndex: newIndex));
  }

  /* void checkPermissions() async {
    final handler = PermissionHandler();
    PermissionStatus status =
        await handler.checkPermissionStatus(PermissionGroup.storage);
    switch (status) {
      case PermissionStatus.granted:
        update(latestState.copyWith(id: HomeStateId.DoneLoading));
        break;
      default:
        final statuses =
            await handler.requestPermissions([PermissionGroup.storage]);
        update(latestState.copyWith(id: HomeStateId.DoneLoading));
        break;
    }
  }*/
}

class HomeState {
  final HomeStateId id;
  final int currentTabIndex;
  final AppDataResponse appData;

  final tabs = [
    TabItem(
        TabItem.OVERVIEW, AssetStrings.homeTabs[0], AssetImages.homeTabs[0]),
    TabItem(
        TabItem.INFORMATION, AssetStrings.homeTabs[1], AssetImages.homeTabs[1]),
    TabItem(
        TabItem.HOSPITAL, AssetStrings.homeTabs[2], AssetImages.homeTabs[2]),
  ];

  HomeState({
    this.id,
    this.currentTabIndex,
    this.appData,
  });

  HomeState copyWith({
    HomeStateId id,
    int currentTabIndex,
    bool downloading,
    AppDataResponse appData,
  }) {
    return HomeState(
      id: id ?? this.id,
      currentTabIndex: currentTabIndex ?? this.currentTabIndex,
      appData: appData ?? this.appData,
    );
  }
}

enum HomeStateId { Loading, DoneLoading, NeedLogin }

class TabItem {
  static const OVERVIEW = 0;
  static const INFORMATION = 1;
  static const HOSPITAL = 2;

  final int id;
  final String title;
  final IconData icon;

  const TabItem(this.id, this.title, this.icon);
}
